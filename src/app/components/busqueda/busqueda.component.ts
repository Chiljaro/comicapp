import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HerosService, Hero } from '../../services/heros.service';
import { ActivatedRoute } from '@angular/router';
import { log } from 'util';



@Component({
  selector: 'app-busqueda',
  templateUrl: './busqueda.component.html'
})
export class BusquedaComponent implements OnInit {
  heros: Hero[] = [];
  termino: string;

  constructor(  private _herosService: HerosService,
                private router: Router,
                private activateRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activateRoute.params.subscribe( params => {
      this.termino = params['termino'];
      this.heros = this._herosService.searchHeros( params['termino'] );
      console.log(this.heros);
    });
  }

  seeHero( idx: number) {
    this.router.navigate( ['/hero', idx] );
  }
}
