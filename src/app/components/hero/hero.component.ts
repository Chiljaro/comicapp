import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HerosService } from '../../services/heros.service';


@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
})
export class HeroComponent {

  hero: any = {};

  constructor( private activateRoute: ActivatedRoute,
               private _herosServices: HerosService) {
    this.activateRoute.params.subscribe( params => {
      this.hero = this._herosServices.getHero( params['id'] );
      console.log(this.hero);
    } );
   }

}
